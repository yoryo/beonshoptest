// problema 1
/*
Escribir una función recursivaque tenga como entrada un array de tipo (1,2,3,4,5,6)
y tenga como salida lo siguiente:
<1><2><3><4><5><6></6></5></4></3></2></1>
*/

function elementBuilder(arrayElements = [], separator, recurse = true) {
  if (recurse) {
    arrayElements.forEach((element, key) => {
      if (separator) {
        console.log(`<${separator}${element}>`);
      } else {
        console.log(`<${element}>`);
      }
      if (key == arrayElements.length - 1) {
        elementBuilder(arrayElements, "/", false);
      }
    });
  } else {
    arrayElements.reverse().forEach((reversedElement) => {
      if (separator) {
        console.log(`<${separator}${reversedElement}>`);
      } else {
        console.log(`<${reversedElement}>`);
      }
    });
  }
}

elementBuilder([1, 2, 3, 10, 15, 4, 30]);

const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

readline.question("finalizando el script", (name) => {
  readline.close();
});
