//problema 2
/*
Problema 2:Partiendo del siguiente string:
"<a><b><c><d><e><f></f></e></d></c></b></a>"
Escribir un programa que produzca la siguiente salida:
<a><b><c><d><e><f></f></e></d></c></b></a>
*/
const elements = "<a><b><c><d><e><f></f></e><d/></c></b></a>";
const cleanedStrings = elements.split("");

const arrayOfStrings = [
  ...new Set(
    cleanedStrings.filter((element) => {
      if (element !== ">" && element !== "<" && element !== "/") {
        return element;
      }
    })
  ),
];

arrayOfStrings.forEach((element, key) => {
  console.log(`<${element}>`);
  if (key == arrayOfStrings.length - 1) {
    arrayOfStrings.reverse().forEach((reversedElement) => {
      console.log(`</${reversedElement}>`);
    });
  }
});

const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

readline.question("finalizando el script", (name) => {
  readline.close();
});
