Se agrego el store de redux con sus action types
y sus reducers.

Se utilizan estados locales para la actualizacion de los mismos con hooks de react

###Primeros pasos

####Instalar dependencias

`$ npm install`

####Ejecutar json server
En la raiz del proyecto ejecutar:

`$ cd src`

Una vez en src ejecutar:

`$ json-server --watch db.json`

####Iniciar proyecto

Para iniciar el proyecto ejecutar.

`$ npm start`

Saldra un aviso sobre que el puerto ya esta en uso, escoger arrancar en otro puerto

####Estructura del proyecto

- src
  - Containers
    -Home.j
  - Store
    -actions
    -config
    -reducers
    -types

* App.js
